# sw-architecture-doc

Example arc42 project running in GitPod.

## How to generate PDF and HTML

- `doctoolchain example generatePDF`
- `doctoolchain example generateHTML`

Generated files can be found in `example/build` directory.
