FROM gitpod/workspace-full

USER gitpod

RUN git clone --recursive https://github.com/docToolchain/docToolchain.git docToolchain

RUN cd docToolchain \ 
        && rm -rf .git \
        && rm -rf resources/asciidoctor-reveal.js/.git \
        && rm -rf resources/reveal.js/.git

ENV PATH="/home/gitpod/docToolchain/bin:${PATH}"